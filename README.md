# Dataloader

Example setup:
```kotlin
data class User(val id: Int, val name: String)

val dbTestSource = (0..1000).map { it to User(it, "name-$it") }.toMap()

fun main() {
    val loader = DataLoader<Int, User> { keys ->
        println("Executing -> SELECT NAME FROM PERSON p WHERE p.ID in (${keys.joinToString()})")
        delay(10)
        keys.map { it to dbTestSource[it] }.toMap()
    }


    runBlocking {
        loader.begin {
            start { println("25 -> ${load(25)}") }
            start { println("100 -> ${load(100)}") }
            start { println("50 -> ${load(50)}") }
            start { println("1234 -> ${load(1234)}") }
            start { println("555 -> ${load(555)}") }
            start { println("50 -> ${load(50)}") }
        }
        println()
        loader.begin {
            start { println("50 -> ${load(50)}") }
        }
        println()
        loader.begin {
            start { println("50 -> ${load(50)}") }
            start { println("522 -> ${load(522)}") }
            start { println("555 -> ${load(555)}") }
        }
    }
}
```

prints out:
```
Executing -> SELECT NAME FROM PERSON p WHERE p.ID in (25, 100, 50, 1234, 555)
25 -> User(id=25, name=name-25)
100 -> User(id=100, name=name-100)
50 -> User(id=50, name=name-50)
50 -> User(id=50, name=name-50)
1234 -> null
555 -> User(id=555, name=name-555)

50 -> User(id=50, name=name-50)

Executing -> SELECT NAME FROM PERSON p WHERE p.ID in (522)
50 -> User(id=50, name=name-50)
522 -> User(id=522, name=name-522)
555 -> User(id=555, name=name-555)
```
