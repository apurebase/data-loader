package com.apurebase.dataloader

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

open class DataLoader<K, V>(private val batchLoader: suspend (List<K>) -> Map<K, V?>) {

    private val cache = mutableMapOf<K, V?>()
    private val loadQueue = mutableSetOf<K>()

    private val channels = mutableMapOf<K, Stack<Channel<V?>>>()

    inner class DataScope(scope: CoroutineScope): CoroutineScope by scope {
        private var counter: AtomicInteger = AtomicInteger(0)

        suspend fun start(block: suspend CoroutineScope.() -> Unit) {
            counter.incrementAndGet()
            launch { block() }
        }

        suspend fun load(key: K): V? {
            loadQueue.add(key)
            val channel = Channel<V?>()
            channels.getStack(key).add(channel)
            val value = async { channel.receive(). also { channel.close() } }
            if (counter.decrementAndGet() == 0) join()
            return value.await()
        }
    }

    suspend fun begin(block: suspend DataScope.() -> Unit) = coroutineScope {
        block(DataScope(this))
    }

    private suspend fun join() {
        val toLoad = loadQueue
            .filterNot { cache.containsKey(it) }
            .toList()

        loadQueue.clear()

        // TODO: Exception handling
        if (toLoad.isNotEmpty()) {
            batchLoader(toLoad).forEach { (key, value) -> cache[key] = value }
        }

        channels.forEach { (key, channel) ->
            while (channel.isNotEmpty()) {
                channel.pop().send(cache[key])
            }
        }
        channels.clear()
    }

    private fun MutableMap<K, Stack<Channel<V?>>>.getStack(key: K): Stack<Channel<V?>> {
        if (!this.containsKey(key)) put(key, Stack())
        return get(key)!!
    }
}
